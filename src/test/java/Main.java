import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.html.HTMLSelectElement;

public class Main {
    WebDriver driver = new FirefoxDriver();
    @BeforeClass
    public static void beforeClass () {
        System.setProperty("webdriver.gecko.driver", "D:\\Home_Work\\geckodriver.exe");
    }
    @Test
    public void openUserData () throws InterruptedException {

        driver.get ("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).click(); //Переход на другую вкладку
        driver.findElement(By.cssSelector("#first_name")).sendKeys("Ivan"); // Ввод текста в поле Имя
        driver.findElement(By.cssSelector("#last_name")).sendKeys("Ivanov"); // Ввод текста в поле Фамилия
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys("380481111111"); // Ввод раб телефона
        driver.findElement(By.cssSelector("#field_phone")).sendKeys("380671111111"); // Ввод моб телефона
        driver.findElement(By.cssSelector("#field_email")).sendKeys("ivanov123@gmail.com"); // Ввод эл. адреса
        driver.findElement(By.cssSelector("#field_password")).sendKeys("Qwerty123"); // Ввод пароля
        driver.findElement(By.cssSelector("#male")).click(); // Пол
        new Select (driver.findElement(By.cssSelector("#position"))).selectByVisibleText("qa"); // Выбор позиции
        driver.findElement(By.cssSelector("#button_account")).click(); //Регистрация

    }

}
